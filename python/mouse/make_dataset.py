import argparse
import os
import sys
import random
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

#assume code is running from root of the project folder (where config.py is)
#that is the operating system current working dir is the SiameseAllen project folder
sys.path.append(os.getcwd())
from config import *
from python.mouse.utils import utils


"""
Creates train, test, validation, and duplicates holdout examples for a siamese binary classifier.
    
TODO:
    - create negative image pairs that are more similar
"""

NP_RATIO = 2
FLIP_PERCENTAGE = 0.5
IMAGE_INFO_FN = 'imageInfo_cleaned.csv'
EXAMPLES_FN = 'examples.csv'


def make_image_pairs(img_info, exclude_same_experiments=False):
    """
    Creates image pairs for a siamese binary classifier using images described in img_info, satisfying the following:
        - label = 0 if images test different genes
        - label = 1 if images test same gene (and are from different experiments if exclude_same_experiments=True)
        - there are exactly N 1-labels and N*NP_RATIO 0-labels
        - each image in img_info is roughly equally represented in the image pairs
        - FLIP_PERCENTAGE % of the image pairs are "flipped" (first image in the pair has img_info['image_set']==2)

    :param img_info: pandas dataframe describing the dataset of ISH experiments.
    :param exclude_same_experiments: if True, remove positive examples where both images are from the same experiment
    :return: pandas dataframe where each row describes a pair of images and a label 0 or 1.
    """
    # inner join image_set 1 with image_set 2 on gene_symbol
    t1 = img_info[img_info['image_set'] == 1].set_index('gene_symbol')
    t2 = img_info[img_info['image_set'] == 2].set_index('gene_symbol')
    t = t1.join(t2, how='inner', lsuffix='1', rsuffix='2')
    if exclude_same_experiments:
        t = t[t['experiment_id1'] != t['experiment_id2']].sort_values(by=['image_id1', 'image_id2'])
        img_info = img_info[img_info['gene_symbol'].isin(t.index)].sort_values(by='image_id')

    if len(set(t.index)) < 2:
        raise ValueError('Only 1 possible gene symbol - no negative examples')

    # N positive examples
    positives = pd.DataFrame({
        'image_id1': list(t['image_id1']),
        'gene_symbol1': list(t.index),
        'image_id2': list(t['image_id2']),
        'gene_symbol2': list(t.index),
        'label': 1
    })

    # N * NP_RATIO negative examples
    negatives = pd.DataFrame(columns=positives.columns, index=np.arange(positives.shape[0] * NP_RATIO))
    q = []
    for i1 in range(positives.shape[0]):
        image_id1 = positives.iloc[i1]['image_id1']
        gene_symbol1 = positives.iloc[i1]['gene_symbol1']
        added = []
        while len(added) < NP_RATIO:
            if len(q) == 0:
                q = list(np.where(img_info['image_set'] == 2)[0])
                q.sort()
                random.shuffle(q)
            i2 = q.pop()
            image_id2 = img_info.iloc[i2]['image_id']
            gene_symbol2 = img_info.iloc[i2]['gene_symbol']
            if gene_symbol1 != gene_symbol2 and image_id2 not in added:
                negatives.iloc[NP_RATIO * i1 + len(added)] = {
                    'image_id1': image_id1,
                    'gene_symbol1': gene_symbol1,
                    'image_id2': image_id2,
                    'gene_symbol2': gene_symbol2,
                    'label': 0
                }
                added += [image_id2]

    # randomly flip image1 and image2
    pairs = positives.append(negatives, ignore_index=True)
    i = random.sample(list(pairs.index), int(len(pairs.index) * FLIP_PERCENTAGE))
    pairs.loc[i, ['image_id1', 'gene_symbol1', 'image_id2', 'gene_symbol2']] = \
        pairs.loc[i, ['image_id2', 'gene_symbol2', 'image_id1', 'gene_symbol1']].values

    return pairs, img_info


def make_dataset(img_info,
                 percent_test,
                 percent_validation,
                 holdout_duplicates,
                 random_state):
    """
    Splits genes found in img_info into train, test, validation, and duplicates_holdout sets and creates image pairs.

    :param img_info: pandas dataframe describing the dataset of ISH experiments.
    :param percent_test: proportion of genes to use for test set
    :param percent_validation: proportion of genes to use for validation set
    :param holdout_duplicates: if True, holdout "duplicate genes". These are excluded when partitioning train, test,
                               and validation genes and are made into a 4th "duplicates holdout" set.
    :param random_state: for random.seed()
    :return: a pandas dataframe where each row describes a pair of images, a label (0 or 1), and a data_set ('train',
             'test', 'validation', and optionally 'duplicates-holdout')
    """
    random.seed(random_state)

    # holdout duplicate genes, make their examples excluding same experiment examples
    duplicates = None
    if holdout_duplicates:
        duplicates_info, duplicates_size = utils.get_gene_experiment_duplicates(img_info)
        img_info = img_info[~img_info['gene_symbol'].isin(duplicates_size['gene_symbol'])]
        duplicates, _ = make_image_pairs(duplicates_info, exclude_same_experiments=True)

    # split train, test, validation sets
    genes = list(set(img_info['gene_symbol']))
    genes.sort()
    t, validation_genes = train_test_split(genes, test_size=percent_validation, random_state=random_state)
    train_genes, test_genes = train_test_split(t, test_size=percent_test / (1 - percent_validation),
                                               random_state=random_state)
    train_info = img_info[img_info['gene_symbol'].isin(train_genes)]
    test_info = img_info[img_info['gene_symbol'].isin(test_genes)]
    validation_info = img_info[img_info['gene_symbol'].isin(validation_genes)]

    # make train, test, validation examples
    train, _ = make_image_pairs(train_info)
    test, _ = make_image_pairs(test_info)
    validation, _ = make_image_pairs(validation_info)

    # combine into dataframe
    train['data_set'] = ['train'] * train.shape[0]
    test['data_set'] = ['test'] * test.shape[0]
    validation['data_set'] = ['validation'] * validation.shape[0]
    ret = (train.append(test)).append(validation)
    if duplicates is not None:
        duplicates['data_set'] = ['duplicates_holdout'] * duplicates.shape[0]
        ret = ret.append(duplicates)
    ret.index = np.arange(len(ret.index))

    return ret


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-noprompt', action='store_const', const=True, default=False)
    parser.add_argument('-rseed',
                        help='for random.seed()', type=int)
    parser.add_argument('-test',
                        help='percentage of examples to use for test set; must be in [0,1]', type=float,
                        default=0.18)
    parser.add_argument('-vali',
                        help='percentage of examples to use for validation set; must be in [0,1]', type=float,
                        default=0.10)
    parser.add_argument('-dup_holdout', action='store_const', const=True,
                        help='create duplicate_holdout set in addition to train, test, validation',
                        default=True)
    args = parser.parse_args()

    if args.test < 0 or args.test > 1:
        raise ValueError('test percentage must be in [0,1]: --test ' + args.test)

    if args.vali < 0 or args.vali > 1:
        raise ValueError('validation percentage must be in [0,1]: --vali ' + args.vali)

    if args.test + args.vali > 1:
        raise ValueError('test and validation percentages cannot sum to more than 1: --test %s --vali %s'
                         % (args.test, args.vali))

    image_info_fn = os.path.join(MOUSE_DATA_DIR, MOUSE_REGION.replace(' ', '_').lower(), IMAGE_INFO_FN)
    examples_fn = os.path.join(MOUSE_DATA_DIR, EXAMPLES_FN)

    print('-------------------------------------------------------------------')
    print('%20s: %s' % ('INPUT IMAGE_INFO', image_info_fn))
    print('%20s: %s' % ('OUTPUT EXAMPLES', examples_fn))
    for k, v in vars(args).items():
        print('%20s: %s' % (k, v))

    if not args.noprompt:
        ans = input('Are these arguments correct? [y/n] ')
        if not ans.lower().startswith('y'):
            exit()
    print('-------------------------------------------------------------------')

    image_info = pd.read_csv(image_info_fn)
    examples = make_dataset(
        img_info=image_info,
        percent_test=args.test,
        percent_validation=args.vali,
        holdout_duplicates=args.dup_holdout,
        random_state=args.rseed)  # 123

    examples.to_csv(examples_fn, na_rep='NaN', index=False)
    print(examples.shape[0], 'total examples written to', examples_fn, ':')
    for data_set in set(examples['data_set']):
        print('%20s: %6d examples' % (data_set, examples[examples['data_set'] == data_set].shape[0]))
