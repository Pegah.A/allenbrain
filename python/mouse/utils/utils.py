import os
import imageio
import pandas as pd
import matplotlib.pyplot as plt
import glob
import numpy as np
import sys

sys.path.append(os.getcwd())
from config import *


def brain_region_name(brain_region=MOUSE_REGION):
    return brain_region.replace(' ', '_').lower()


def get_image_info(fn='imageInfo_cleaned.csv'):
    return pd.read_csv(os.path.join(MOUSE_DATA_DIR, brain_region_name(), fn))


def get_examples(fn='examples.csv'):
    return pd.read_csv(os.path.join(MOUSE_DATA_DIR, fn))


def get_examples_unpack(fn='examples.csv'):
    examples = get_examples(fn)
    return (examples[examples['data_set'] == x] for x in ('train', 'test', 'validation', 'duplicates_holdout'))


def get_raw_image(image_id):
    return imageio.imread(os.path.join(MOUSE_DATA_DIR, 'raw', str(image_id)+'.jpg'))


def get_whole_brain_image(image_id,
                          dimensions='%dx%d' % (MOUSE_WIDTH, MOUSE_HEIGHT)):
    return imageio.imread(os.path.join(MOUSE_DATA_DIR, 'whole_brain', dimensions, str(image_id)+'.jpg'))


def get_brain_region_crop(image_id,
                          dimension=MOUSE_PATCH_SIZE):
    return imageio.imread(os.path.join(MOUSE_DATA_DIR, brain_region_name(), str(dimension), str(image_id)+'.jpg'))


def get_gene_experiment_duplicates(img_info):
    genes = img_info[img_info['image_set'] == 1].groupby('gene_symbol').size().reset_index(name='n_experiments')
    duplicates = genes[genes['n_experiments'] > 1]
    return img_info[img_info['gene_symbol'].isin(duplicates['gene_symbol'])], duplicates


def plot_grid(nrow, ncol, subplot_size, images, titles=[]):
    ratio = images[0].shape[0] / images[0].shape[1]
    fig, axes = plt.subplots(nrows=nrow, ncols=ncol, figsize=[ncol * subplot_size, nrow * subplot_size * ratio])
    axes = axes.ravel()

    for i in range(len(axes)):
        ax = axes[i]
        ax.imshow(images[i])
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
        if len(titles) > 0:
            ax.set_title(titles[i])
        for spine in ax.spines.keys():
            ax.spines[spine].set_visible(False)

    return fig


class _Collection:
    pass


def load_brain_region_crops_dataset():
    img_dir = os.path.join(MOUSE_DATA_DIR, brain_region_name(), str(MOUSE_PATCH_SIZE))
    return load_dataset(img_dir)


def load_whole_brain_images_dataset():
    img_dir = os.path.join(MOUSE_DATA_DIR, 'whole_brain', '%dx%d' % (MOUSE_WIDTH, MOUSE_HEIGHT))
    return load_dataset(img_dir)


def load_dataset(img_dir):
    examples = get_examples()
    print('found %d examples in %s' % (examples.shape[0], os.path.join(MOUSE_DATA_DIR, 'examples.csv')))
    print('loading %d images found in %s' % (len(glob.glob(os.path.join(img_dir, '*.jpg'))), img_dir), '...')

    images1 = np.empty((examples.shape[0], MOUSE_PATCH_SIZE, MOUSE_PATCH_SIZE, 3), dtype=np.uint8)
    images2 = np.empty((examples.shape[0], MOUSE_PATCH_SIZE, MOUSE_PATCH_SIZE, 3), dtype=np.uint8)
    for i, row in examples.iterrows():
        img_fn = os.path.join(img_dir, str(row['image_id1']) + '.jpg')
        img = imageio.imread(img_fn)
        images1[i, :, :, :] = np.array(img)

        img_fn = os.path.join(img_dir, str(row['image_id2']) + '.jpg')
        img = imageio.imread(img_fn)
        images2[i, :, :, :] = np.array(img)
    print('done.')

    l = []
    for s in ['train', 'test', 'validation', 'duplicates_holdout']:
        sel = examples['data_set'] == s
        c = _Collection()
        c.labels = np.array(examples[sel]['label'])
        c.images1 = images1[sel, ...]
        c.images2 = images2[sel, ...]
        c.genes1 = np.array(examples[sel]['gene_symbol1'])
        c.genes2 = np.array(examples[sel]['gene_symbol2'])
        l.append(c)

    data = _Collection()
    data.train = l[0]
    data.test = l[1]
    data.validation = l[2]
    data.duplicates = l[3]

    return data
