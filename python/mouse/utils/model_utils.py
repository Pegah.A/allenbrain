from sklearn import metrics
import glob
import re
import numpy as np
import os
import pickle
import sys

sys.path.append(os.getcwd())
from config import *


def score_auc(y_true, y_pred):
    fpr, tpr, thresholds = metrics.roc_curve(y_true, y_pred, pos_label=1)
    auc = metrics.auc(fpr, tpr)
    return auc, (fpr, tpr, thresholds)


def score_acc(y_true, y_pred):
    return metrics.accuracy_score(y_true=y_true, y_pred=y_pred)


class Batch:
    def __init__(self):
        self.start = None
        self.idx = None


def do_batch(x0, x1, y, total, batch, batch_size):
    if (batch.start is None) or (batch.start + batch_size > total):
        batch.start = 0
        batch.idx = np.arange(total)
        np.random.shuffle(batch.idx)

    i = batch.idx[batch.start:batch.start + batch_size]
    batch.start += batch_size
    return x0[i, ...], x1[i, ...], y[i]


def get_model_save_fns(model_name):
    path = os.path.join(MOUSE_MODEL_DIR, model_name)
    saved_model_fns = [fn.strip('.meta') for fn in glob.glob(os.path.join(path, '*.meta'))
                       if re.search('model_save-[1-9]+\\.meta$', fn) is not None]
    return saved_model_fns, parse_steps(saved_model_fns)


def parse_steps(saved_model_fns):
    return [int(re.search(r'\d+$', fn).group()) for fn in saved_model_fns]


def get_model_args(model_name):
    args_fn = os.path.join(MOUSE_MODEL_DIR, model_name, 'args.pickle')
    with open(args_fn, 'rb') as f:
        return pickle.load(f)
