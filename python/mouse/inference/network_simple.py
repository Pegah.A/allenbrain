import tensorflow as tf
from python.mouse.inference.siamese import Network
from python.mouse.inference import network_layers


def cnn(input_images):
    """
    :param input_images: input images tensor of shape [batch, in_height, in_width, in_channels=1]
    :return: last layer of model, and Siamese with attributes parameters and layers
    """
    model = Network()
    model.add(input_images)

    # conv1
    with tf.variable_scope('conv1'):
        conv1, _ = network_layers.add_conv2d(
            inputs=input_images,
            kernel_shape=[5, 5, 3, 20],
            strides=[1, 1, 1, 1],
            padding='VALID',
            name='conv1',
            model=model
        )

    # pool1
    pool1 = tf.nn.max_pool(
        value=conv1,
        ksize=[1, 2, 2, 1],
        strides=[1, 2, 2, 1],
        padding='VALID',
        name='pool1'
    )
    model.add(pool1)

    # conv2
    with tf.variable_scope('conv2'):
        conv2, _ = network_layers.add_conv2d(
            inputs=pool1,
            kernel_shape=[5, 5, 20, 50],
            strides=[1, 1, 1, 1],
            padding='VALID',
            name='conv2',
            model=model
        )

    # pool2
    pool2 = tf.nn.max_pool(
        value=conv2,
        ksize=[1, 2, 2, 1],
        strides=[1, 1, 1, 1],
        padding='VALID',
        name='pool2'
    )
    model.add(pool2)

    return pool2, model
