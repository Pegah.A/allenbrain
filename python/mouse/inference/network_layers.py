import tensorflow as tf


def add_fully_connected(inputs, units, name, model):
    """
    :param inputs: input tensor
    :param units: number of units
    :param name: name of tensor
    :param model: a Siamese.Network to build
    :return: layer, [params]
    """
    assert len(inputs.get_shape()) == 2

    input_shape = inputs.get_shape().as_list()
    weights = tf.Variable(
        initial_value=tf.truncated_normal(shape=[input_shape[len(input_shape) - 1], units], dtype=tf.float32,
                                          stddev=1e-1),
        trainable=True,
        name=name + '/weights'
    )
    biases = tf.Variable(
        initial_value=tf.constant(0., shape=[units], dtype=tf.float32),
        trainable=True,
        name=name + '/bias'
    )
    fc = tf.nn.bias_add(tf.matmul(inputs, weights), biases, name=name)

    model.add(layer=fc, params=[weights, biases])
    return fc, [weights, biases]


def add_conv2d(inputs, kernel_shape, strides, padding, name, model):
    """
    :param inputs: input tensor
    :param kernel_shape: shape of filters - [filter_height, filter_width, in_channels, out_channels]
    :param strides: stride of filters
    :param padding: padding of filters
    :param name: name of tensor
    :param model: a Siamese.Network to build
    :return: layer, [params]
    """
    # kernel shape:
    var_kernel = tf.Variable(
        initial_value=tf.truncated_normal(shape=kernel_shape, dtype=tf.float32, stddev=1e-1),
        name='kernel'
    )
    conv = tf.nn.conv2d(
        input=inputs,
        filter=var_kernel,
        strides=strides,
        padding=padding
    )
    var_bias = tf.Variable(
        initial_value=tf.constant(0., shape=[kernel_shape[3]], dtype=tf.float32),
        trainable=True,
        name='bias'
    )
    conv_plus_bias = tf.nn.bias_add(conv, var_bias)
    layer_conv = tf.nn.relu(conv_plus_bias, name=name)

    model.add(layer=layer_conv, params=[var_kernel, var_bias])
    return layer_conv, [var_kernel, var_bias]


def add_conv_transform_100(inputs, model, name=''):
    """
    Flatten + Fully-Connected (500 units) + Fully-Connected (100 units)
    :param inputs: input tensor
    :param model: a Siamese.Network to build
    :param name: name of tensor
    :return: last layer (Fully-Connected with 100 units)
    """
    flat = tf.contrib.layers.flatten(inputs, scope=name + 'flat')
    model.add(layer=flat)
    fc1, fc1_p = add_fully_connected(
        inputs=flat,
        units=500,
        name=name + 'fc1',
        model=model
    )
    fc2, fc2_p = add_fully_connected(
        inputs=fc1,
        units=100,
        name=name + 'fc2',
        model=model
    )
    return fc2


def get_scaled_tensor(inputs, name, vmin=0., vmax=1.):
    """
    :param inputs: input tensor
    :param name: name of tensor
    :param vmin: min value for scaling
    :param vmax: max value for scaling
    :return: a tensor to scale inputs to the interval of [vmin, vmax]
    """
    inputs_min = tf.reduce_min(inputs)
    inputs_max = tf.reduce_max(inputs)
    if inputs_min == inputs_max:
        return inputs

    scaled = (inputs - inputs_min) / (inputs_max - inputs_min)
    scaled = tf.add(scaled * (vmax - vmin), vmin, name=name)
    return scaled
