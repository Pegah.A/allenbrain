import tensorflow as tf
import pickle
from itertools import chain
import os
import sys

sys.path.append(os.getcwd())
from python.mouse.inference import network_layers


class Network:
    def __init__(self):
        self.layers = []
        self.layers_to_params = {}

    def add(self, layer, params=None):
        self.layers.append(layer)
        if params is not None:
            self.layers_to_params[layer.name] = params

    def get(self, name):
        for layer in self.layers:
            if layer.name.endswith(name):
                return layer
        return None

    def input(self):
        return self.layers[0]

    def output(self):
        return self.layers[len(self.layers) - 1]

    def get_param(self, name):
        for param in self.layers_to_params.values():
            if param.name.endswith(name):
                return param
        return None

    def get_params_for(self, layer_name):
        if layer_name in self.layers_to_params:
            return self.layers_to_params[layer_name]
        return None

    def __str__(self):
        n = len(self.layers[0].name) + 5
        s = ''
        for layer in self.layers:
            s += ('%' + str(n) + 's: %s\n') % (layer.name, layer.get_shape().as_list())
        return s.strip('\n')

    def to_pickle(self, fn):
        s_layers = [layer.name for layer in self.layers]
        s_layer_to_params = {k: [param.name for param in v] for k, v in self.layers_to_params.items()}
        with open(fn, 'wb') as f:
            pickle.dump((s_layers, s_layer_to_params), f)

    @staticmethod
    def from_pickle(fn):
        with open(fn, 'rb') as f:
            s_layers, s_layer_to_params = pickle.load(f)
            p = {name: tf.get_default_graph().get_tensor_by_name(name)
                 for name in list(chain.from_iterable(s_layer_to_params.values()))}
            ret = Network()
            ret.layers_to_params = {k: [p[name] for name in v] for k, v in s_layer_to_params.items()}
            ret.layers = [tf.get_default_graph().get_tensor_by_name(name) for name in s_layers]
            return ret


class Siamese:

    def __init__(self, model_dir, net0, net1, margin, threshold):
        if model_dir is not None:
            self.model_dir = model_dir
            self.net0 = net0
            self.net1 = net1
            self.margin = tf.Variable(initial_value=margin, trainable=False, name='margin', dtype=tf.float32)
            self.threshold = tf.Variable(initial_value=threshold, trainable=False, name='threshold', dtype=tf.float32)

            # training and predictions
            self.y = tf.placeholder(dtype=tf.float32, shape=[None], name='y')
            self.loss, self.distance_0 = contrastive_loss(self.net0.output(), self.net1.output(), self.y, self.margin)
            self.distance = network_layers.get_scaled_tensor(self.distance_0, 'scaled_distance')
            self.prediction = tf.to_int32(self.distance < self.threshold, name='prediction')

            # metrics
            _, self.auc = tf.metrics.auc(labels=self.y, predictions=(1 - self.distance), name='auc')
            _, self.accuracy = tf.metrics.accuracy(labels=self.y, predictions=self.prediction)

    def __str__(self):
        s = '[---------' + super(Siamese, self).__str__() + '---------\n'
        s += ' model name: %s\n' % str(self.model_dir)
        s += ' net0:\n%s\n'      % str(self.net0)
        s += ' net1:\n%s\n'      % str(self.net1)
        s += '-----------------------------------------------------------------]'
        return s

    def to_pickle(self):
        self.net0.to_pickle(os.path.join(self.model_dir, 'siamese_net0.pickle'))
        self.net1.to_pickle(os.path.join(self.model_dir, 'siamese_net1.pickle'))
        d = {
            'model_dir'  : self.model_dir,
            'margin'     : self.margin.name,
            'threshold'  : self.threshold.name,
            'y'          : self.y.name,
            'loss'       : self.loss.name,
            'distance_0' : self.distance_0.name,
            'distance'   : self.distance.name,
            'prediction' : self.prediction.name,
            'auc'        : self.auc.name,
            'accuracy'   : self.accuracy.name
        }
        with open(os.path.join(self.model_dir, 'siamese.pickle'), 'wb') as f:
            pickle.dump(d, f)

    @staticmethod
    def from_pickle(model_dir):
        net0 = Network.from_pickle(os.path.join(model_dir, 'siamese_net0.pickle'))
        net1 = Network.from_pickle(os.path.join(model_dir, 'siamese_net1.pickle'))
        with open(os.path.join(model_dir, 'siamese.pickle'), 'rb') as f:
            d = pickle.load(f)
        siamese = Siamese(None, None, None, None, None)
        siamese.model_dir = model_dir
        siamese.net0 = net0
        siamese.net1 = net1
        siamese.margin = tf.get_default_graph().get_tensor_by_name(d['margin'])
        siamese.threshold = tf.get_default_graph().get_tensor_by_name(d['threshold'])
        siamese.y = tf.get_default_graph().get_tensor_by_name(d['y'])
        siamese.loss = tf.get_default_graph().get_tensor_by_name(d['loss'])
        siamese.distance_0 = tf.get_default_graph().get_tensor_by_name(d['distance_0'])
        siamese.distance = tf.get_default_graph().get_tensor_by_name(d['distance'])
        siamese.prediction = tf.get_default_graph().get_tensor_by_name(d['prediction'])
        siamese.auc = tf.get_default_graph().get_tensor_by_name(d['auc'])
        siamese.accuracy = tf.get_default_graph().get_tensor_by_name(d['accuracy'])
        return siamese


def contrastive_loss(out0, out1, y_true, margin):
    with tf.name_scope('contrastive_loss'):
        d = tf.reduce_sum(tf.square(tf.subtract(out0, out1)), 1)
        d_sqrt = tf.sqrt(1e-6 + d)
        loss = (y_true * d) + ((1 - y_true) * tf.square(tf.maximum(tf.subtract(margin, d_sqrt), 0)))
        loss = tf.reduce_mean(loss)  # Note: constant component removed (/2)
        return loss, d_sqrt


def update_threshold(siamese, new_threshold, sess):
    sess.run(siamese.threshold.assign(new_threshold))


def get_summaries_all(siamese):
    tf.summary.histogram('net0_output_layer_activations', siamese.net0.output())
    tf.summary.histogram('net1_output_layer_activations', siamese.net1.output())

    tf.summary.histogram('distance_0', siamese.distance_0)
    tf.summary.histogram('distance', siamese.distance)

    tf.summary.scalar('auc', siamese.auc)
    tf.summary.scalar('accuracy', siamese.accuracy)
    tf.summary.scalar('loss', siamese.loss)

    for var in tf.trainable_variables():
        tf.summary.histogram(var.op.name, var)

    merged = tf.summary.merge_all()
    return merged
