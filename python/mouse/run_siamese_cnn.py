import tensorflow as tf
from datetime import datetime
import os
import sys
import numpy as np
import argparse
import csv

sys.path.append(os.getcwd())
from config import *
from python.mouse.utils import model_utils
from python.mouse.utils import utils
from python.mouse.inference import siamese
from python.mouse.inference import network_layers, network_simple

USE_CROPS = True
SHARE_WEIGHTS = True
MARGIN = 100
THRESHOLD = 0.3
LEARNING_RATE = 0.001
BATCH_SIZE = 1000

SAVE_EVERY = 100
REPORT_EVERY = 10
MAX_SAVE_FILES = 5

# TODO learn with decay
# TODO test acc, auc


def build_network(x):
    out1, net = network_simple.cnn(x)
    out2 = network_layers.add_conv_transform_100(inputs=out1, model=net)
    return net


def build_siamese(model_dir,
                  input_shape,
                  share_weights=SHARE_WEIGHTS,
                  margin=MARGIN,
                  threshold=THRESHOLD):

    with tf.variable_scope('siamese'):
        x0 = tf.placeholder(dtype=tf.float32, shape=input_shape, name='x0')
        x1 = tf.placeholder(dtype=tf.float32, shape=input_shape, name='x1')

        if share_weights:
            with tf.variable_scope('net') as scope:
                net0 = build_network(x0)
                scope.reuse_variables()
                # TODO not reusing??
                net1 = build_network(x1)
        else:
            with tf.variable_scope('net0'):
                net0 = build_network(x0)
            with tf.variable_scope('net1'):
                net1 = build_network(x1)

        return siamese.Siamese(
            model_dir=model_dir,
            net0=net0,
            net1=net1,
            margin=margin,
            threshold=threshold
        )


def initialize_siamese(model, sess, args):
    global_step = tf.Variable(initial_value=0, name='global_step', trainable=False)
    learning_rate = tf.constant(value=args['learning_rate'], name='learning_rate')
    train_op = tf.train.AdamOptimizer(learning_rate=learning_rate) \
        .minimize(model.loss, global_step=global_step)

    tf.add_to_collection('train_op', train_op)
    tf.add_to_collection('global_step', global_step)

    init = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
    sess.run(init)

    return train_op, global_step


def restore_siamese(sess, model_dir, saved_model_fn):

    # restore tensorflow metagraph
    saver = tf.train.import_meta_graph(os.path.join(model_dir, saved_model_fn + '.meta'))
    saver.restore(sess, os.path.join(model_dir, saved_model_fn))
    train_op = tf.get_collection('train_op')[0]
    global_step = tf.get_collection('global_step')[0]

    # restore Siamese object
    model = siamese.Siamese.from_pickle(model_dir)

    # tf.metrics.auc and tf.metrics.accuracy have uninitialized local variables after import_meta_graph
    # https://github.com/tensorflow/tensorflow/issues/9747
    # tf.get_default_graph().clear_collection('queue_runners')
    # tf.get_default_graph().clear_collection('local variables')

    return model, train_op, global_step


def do_eval(sess, model, dataset):
    distance_0_vals, distance_vals, loss_val, auc_val = sess.run(
        fetches=[model.distance_0, model.distance, model.loss, model.auc],
        feed_dict={model.net0.input(): dataset.images1,
                   model.net1.input(): dataset.images2,
                   model.y: dataset.labels}
    )
    print('validation: mean distance %f (%f), loss %f, auc %f (%f)'
          % (np.mean(distance_vals), np.mean(distance_0_vals),
             loss_val,
             auc_val, model_utils.score_auc(dataset.labels, distance_0_vals)[0]))


def run(args, saved_model_fn):

    # ======================================= Load data =======================================
    if args['use_crops']:
        data = utils.load_brain_region_crops_dataset()
        input_shape = [None, MOUSE_PATCH_SIZE, MOUSE_PATCH_SIZE, 3]
    else:
        data = utils.load_whole_brain_images_dataset()
        input_shape = [None, MOUSE_HEIGHT, MOUSE_WIDTH, 3]

    m_train = data.train.labels.shape[0]
    print('%d train, %d test, %d validation, and %d duplicates holdout examples' % (
        data.train.labels.shape[0], data.test.labels.shape[0],
        data.validation.labels.shape[0], data.duplicates.labels.shape[0]
    ))

    # ============================= initialize or restore model ===============================

    sess = tf.Session()
    if saved_model_fn is not None:
        model, train_op, global_step = restore_siamese(sess, args['model_dir'], saved_model_fn)
    else:
        model = build_siamese(model_dir=args['model_dir'],
                              input_shape=input_shape,
                              share_weights=args['share_weights'],
                              margin=args['margin'],
                              threshold=args['threshold'])
        model.to_pickle()
        train_op, global_step = initialize_siamese(model, sess, args)

    # tensorboard
    summary = siamese.get_summaries_all(model)
    writer = tf.summary.FileWriter(os.path.join(MOUSE_MODEL_DIR, model.model_dir, 'train.log'), sess.graph)

    # train saver
    saver = tf.train.Saver(max_to_keep=MAX_SAVE_FILES)

    # ======================================= training ==========================================
    batch = model_utils.Batch()  # TODO use tensorflow
    global_step_val = sess.run(global_step)
    print('starting at global step', global_step_val)
    print('start:', datetime.now())

    for i in range(args['n_steps']):
        global_step_val = sess.run(global_step)

        x0, x1, yt = model_utils.do_batch(
            data.train.images1, data.train.images2, data.train.labels, m_train, batch, args['batch_size'])
        _, loss_val, auc_val, acc_val, summary_str = sess.run(
            fetches=[train_op, model.loss, model.auc, model.accuracy, summary],
            feed_dict={model.net0.input(): x0,
                       model.net1.input(): x1,
                       model.y: yt}
        )
        writer.add_summary(summary_str, global_step=global_step_val)

        if np.isnan(loss_val):
            print('\nDiverged at step %d(%d)' % (i, global_step_val))
            break

        if i % SAVE_EVERY == 0:
            saver.save(sess, os.path.join(model.model_dir, 'model_save'), global_step=global_step_val)

        elif i % REPORT_EVERY == 0:
            print('%s step %d(%d): loss %f, auc %f, accuracy %f'
                  % (datetime.now(), i, global_step_val, loss_val, auc_val, acc_val))

    final_fn = saver.save(sess, os.path.join(model.model_dir, 'model_save'), global_step=global_step_val)
    writer.close()
    sess.close()
    print('final model saved to', final_fn)
    print('finish:', datetime.now())

    # ======================================= evaluate ==========================================
    do_eval(sess, model, data.test)
    do_eval(sess, model, data.validation)
    do_eval(sess, model, data.duplicates)


def process_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=('For new models: trains for NSTEPS starting with initial random state RSEED, '
                     'saving data in the new directory MODEL_NAME.\n'
                     'For existing models: resumes training for model found under MODEL_NAME/SAVED_FN, '
                     'using the initial values of RSEED and NSTEPS.'))
    parser.add_argument(
        'model_name',
        help='directory of model training data',
        type=str
    )
    parser.add_argument(
        '-saved_fn',
        help='name of file from which an existing model will be restored.',
        type=str
    )
    parser.add_argument(
        '-rseed',
        help='initial random state.',
        type=int
    )
    parser.add_argument(
        '-nsteps',
        help='number of steps for training.',
        type=int, default=600
    )
    input_args = parser.parse_args()

    model_dir = os.path.join(MOUSE_MODEL_DIR, input_args.model_name)
    if os.path.isdir(model_dir):
        with open(os.path.join(model_dir, 'args.txt'), 'r') as f:
            reader = csv.reader(f, delimiter=':')
            saved_args = dict(reader)
        args = {
            'model_dir': model_dir,
            'rseed': int(saved_args['rseed']),
            'n_steps': int(saved_args['n_steps']),
            'use_crops': bool(saved_args['use_crops']),
            'share_weights': bool(saved_args['use_crops']),
            'margin': int(saved_args['margin']),
            'threshold': float(saved_args['threshold']),
            'learning_rate': float(saved_args['learning_rate']),
            'batch_size': int(saved_args['batch_size'])
        }
        saved_model_fn = input_args.saved_fn
        if (saved_model_fn is None) or (not os.path.isfile(os.path.join(model_dir, saved_model_fn + '.meta'))):
            raise ValueError('Invalid model save file:', saved_model_fn)
    else:
        os.makedirs(model_dir)
        args = {
            'model_dir': model_dir,
            'rseed': input_args.rseed,
            'n_steps': input_args.nsteps,
            'use_crops': USE_CROPS,
            'share_weights': SHARE_WEIGHTS,
            'margin': MARGIN,
            'threshold': THRESHOLD,
            'learning_rate': LEARNING_RATE,
            'batch_size': BATCH_SIZE
        }
        with open(os.path.join(model_dir, 'args.txt'), 'w') as f:
            writer = csv.writer(f, delimiter=':')
            for key, value in args.items():
                writer.writerow([key, value])
        saved_model_fn = None

    return args, saved_model_fn


if __name__ == '__main__':

    model_args, saved_fn = process_args()
    if saved_fn is None:
        print('Training new model at %s' % model_args['model_dir'])
    else:
        print('Resume training model restored from %s/%s' % (model_args['model_dir'], saved_fn))
    print('--------------------- args.txt ---------------------')
    for k, v in model_args.items():
        print('%s: %s' % (k, v))
    print('----------------------------------------------------')

    ans = input('are these parameters correct? [y/n] ')
    if ans.lower().startswith('y'):
        run(model_args, saved_fn)
