# Lets use this config file for python and R, set variables with = so both can load this:	
# "read it by source("config.py") in R or from config import * in Python."	
# https://ygeneration.website/how-to-use-a-config-file-in-r_and_python/	
	
# ========================================== MOUSE ==========================================	
	
#####testing access

MOUSE_DATA_DIR = "/genome/scratch/Neuroinformatics/lfrench/SiameseAllenData/"	
### test url change	
# brain structure patches	

#MOUSE_REGION = "Retrosplenial area"	
MOUSE_REGION = "Somatomotor areas"

MOUSE_PATCH_SIZE = 256	
	
# whole brain images	
MOUSE_HEIGHT = 256	
MOUSE_WIDTH = 512	

MOUSE_MODEL_DIR = "/Users/jenny/Projects_Python/BCB330/SiameseAllen/results/mouse/models"
