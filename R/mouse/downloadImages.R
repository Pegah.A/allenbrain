# download from Allen Brain Atlas with a time delay between each image

# ======== SETUP ========

source("R/mouse/setup.R")
source("R/mouse/utils.R")

print("saving output to out_downloadImages.txt")
sink("out_downloadImages.txt", split=TRUE)
cat("START:", format(Sys.time()), "\n")


# ======== PARAMETERS =========

REPLACE = FALSE
DELAY = 0.3
MOUSE_VIEW = "projection"
MAX_CONSECUTIVE_ERRORS = 30


# =============================

# images to download
imageIDs = read.table(file.path(MOUSE_DATA_DIR, "raw", "downloads.txt"), stringsAsFactors = FALSE)[,1]
cat("Downloading", length(imageIDs), "images\n")

# don't re-download existing images
if(!REPLACE) {
  existingIDs = list.files(path = file.path(MOUSE_DATA_DIR, "raw"), pattern = "[0-9]+.jpg") %>% gsub(".jpg", "", .) %>% as.numeric
  imageIDs = imageIDs[!imageIDs %in% existingIDs]
  cat("Skipping", length(existingIDs), "existing images\n")
}

initGlobalErrVar()
if(length(imageIDs) > 0) {
  # download images and report any failed
  pb = txtProgressBar(max = length(imageIDs)) 
  images = lapply(seq(length(imageIDs)), function(i) {
    Sys.sleep(DELAY)
    setTxtProgressBar(pb, i)
    imageID = imageIDs[i]
    tryCatch({
      curl::curl_download(
        url = paste0("http://api.brain-map.org/api/v2/image_download/", imageID,
                     "?downsample=", 0,
                     "&view=", MOUSE_VIEW),
        destfile = file.path(MOUSE_DATA_DIR, "raw", paste0(imageID, ".jpg"))
      )
    }, error = function(e) {
      cat("\n", i, format(Sys.time()), imageID, "FAILED TO DOWNLOAD\n")
      print(e)
      updateGlobalErrVar(imageIDs[i], i, MAX_CONSECUTIVE_ERRORS)
      pb = txtProgressBar(max = length(imageIDs), initial = i) 
      return(NULL)
    })
  }) %>% unlist
}

getGlobalAllErrIds() %>% cat("\n", length(.), "ERRORS:", ., "\n")
cat("FINISH:", format(Sys.time()), "\n")
cat(file.path(MOUSE_DATA_DIR, "raw"), ":", length(images), "/", length(imageIDs), " downloaded successfully\n")
sink(NULL)


# END