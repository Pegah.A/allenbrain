# common packages

if(!require("magrittr")) {
  install.packages("magrittr")
  library("magrittr")
}

if(!require("dplyr")) {
  install.packages("dplyr")
  library("dplyr")
}

if(!require("curl")) {
  install.packages("curl")
  library("curl")
}

if(!require("magick")) {
  devtools::install_github("ropensci/magick")
  library("magick")
}

if(!require("allenBrain")) {
  devtools::install_github("oganm/allenBrain")
  library(allenBrain)
}


# global parameters

source("config.py")

# END
